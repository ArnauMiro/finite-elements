c*******************************************************************************************
c
c FINITE ELEMENTS: A finite elements code programmed in Fortran77
c
c ThermSte: Thermal Steady case wrapper
c
c Programmed by: Arnau Miro
c
c*******************************************************************************************

       program FEThermSte

       use System, only: element

       implicit none

c... Variables
       character(LEN=256) :: dir

c... Get current directory
       call getcwd(dir)

c... Read settings
       call ReadSettings(dir)

c... Launch simulation
       select case (element)
            case(1) !Tri3
                call ThermSteTri3(dir)
            case(2)
                call ThermSteQuad4(dir)
            case(3)
                call ThermSteQuad8(dir)
       end select

       end program FEThermSte