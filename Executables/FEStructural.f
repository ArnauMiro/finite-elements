c*******************************************************************************************
c
c FINITE ELEMENTS: A finite elements code programmed in Fortran77
c
c ThermSte: Structural case wrapper
c
c Programmed by: Arnau Miro
c
c*******************************************************************************************

       program FEStructural

       use System, only: element

       implicit none

c... Variables
       character(LEN=256) :: dir

c... Get current directory
       call getcwd(dir)

c... Read settings
       call ReadSettings(dir)

c... Launch simulation
       select case (element)
            case(1) !Tri3
                call StructTri3(dir)
            case(2)
                call StructQuad4(dir)
            case(3)
                call StructQuad8(dir)
       end select

       end program FEStructural