c*******************************************************************************************
c
c Contains output for VTK file format
c
c*******************************************************************************************

       subroutine WriteVTKMesh(directory,filename,n_nodes,eltype)

       use Mesh

       implicit none

c... Dummy Arguments
       character(LEN=*), intent(IN) :: directory,filename
       integer, intent(IN) :: n_nodes, eltype

c... Local containers
       character(LEN=256) :: aux, aux1, aux2
       integer :: i

       open(7,FILE=trim(adjustl(directory))//'/'
     .  //trim(adjustl(filename))//".vtk",FORM='formatted')

c... File header format
       write(7,'(A)') '# vtk DataFile Version 2.0' ! File header
       write(7,'(A)') 'FiniteElements VTK Output'  ! Name of simulation
       write(7,'(A)') 'ASCII'                      ! Datatype ASCII/BINARY
       write(7,'(A)') ' '

c... Node coordinates
       write(7,'(A)') 'DATASET UNSTRUCTURED_GRID'  ! Type of data, in this case a unstructured grid
       write(aux,'(I10)') nnod
       write(7,'(A)') 'POINTS ' // trim(adjustl(aux)) // ' double'      ! nnod points of float type
       do i=1,nnod
            write(aux,'(F30.10)') xnod(1,i)
            write(aux1,'(F30.10)') xnod(2,i)
            write(aux2,'(F30.10)') xnod(3,i)
            write(7,'(A)') trim(adjustl(aux))//'  '//
     .                     trim(adjustl(aux1))//'  '//
     .                     trim(adjustl(aux2))
       end do
       write(7,'(A)') ' '

c... Connectivity
       write(aux,'(I10)') nel
       write(aux1,'(I10)') nel*(n_nodes+1)
       write(7,'(A)') 'CELLS ' // trim(adjustl(aux))
     .                // ' ' // trim(adjustl(aux1))     ! nel elements and size of elnod array
       do i=1,nel
            write(aux,'(I1)') n_nodes
            write(aux1,*) elnod(:,i)-1
            write(7,'(A)') trim(adjustl(aux)) // '  '
     .                     // trim(adjustl(aux1))
       end do
       write(7,'(A)') ' '

c... VTK element type
       write(aux,'(I5)') nel
       write(7,'(A)') 'CELL_TYPES ' // trim(adjustl(aux))
       do i=1,nel
            write(aux,'(I5)') eltype
            write(7,'(A)') trim(adjustl(aux))
       end do
       write(7,'(A)') ' '

c... VTK Data
       write(7,'(A10,1x,I3)') 'POINT_DATA', nnod

       end subroutine WriteVTKMesh

c*******************************************************************************************

       subroutine WriteVTKScalar(directory,filename,var,varl,varname)

       implicit none

c... Dummy Arguments
       character(LEN=*), intent(IN) :: directory,
     .                          filename, varname
       integer, intent(IN) :: varl
       real(kind=8), intent(IN) :: var(varl)

c... Local containers
       character(LEN=256) :: aux, aux1, aux2
       integer :: i

c... Opening file
       open(7,FILE=trim(adjustl(directory))//'/'//
     .      trim(adjustl(filename))//".vtk",
     .      FORM='formatted',ACCESS ='append')

c... Data
       write(7,'(A)') 'SCALARS '//trim(varname)//' double'
       write(7,'(A20)') 'LOOKUP_TABLE default'
       do i=1,varl
            write(7,*) var(i)
       end do

c... Closing file
       close(7)

       end subroutine WriteVTKScalar

c*******************************************************************************************

       subroutine WriteVTKVector(directory,filename,var,varl1,varl2,
     .                           varname)

       implicit none

c... Dummy Arguments
       character(LEN=*), intent(IN) :: directory, filename, varname
       integer, intent(IN) :: varl1, varl2
       real(kind=8), intent(IN) :: var(varl1,varl2)

c... Local containers
       character(LEN=256) :: aux, aux1, aux2
       integer :: i

c... Opening file
       open(7,FILE=trim(adjustl(directory))//'/'//
     .      trim(adjustl(filename))//".vtk",
     .      FORM='formatted',ACCESS ='append')

c... Data
       write(7,'(A)') 'VECTORS '//trim(varname)//' float'
       do i=1,varl2
            write(aux,*) var(:,i)
            write(7,'(A)') trim(adjustl(aux))
       end do

c... Closing file
       close(7)

       end subroutine WriteVTKVector

c*******************************************************************************************

       subroutine WriteVTKDeformed(directory,filename,n_nodes,eltype,
     .                             disp)

       use Mesh

       implicit none

c... Dummy Arguments
       character(LEN=*), intent(IN) :: directory, filename
       integer, intent(IN) :: n_nodes, eltype
       real(kind=8), intent(IN) :: disp(3,nnod)

c... Local containers
       character(LEN=256) :: aux, aux1, aux2
       integer :: i

c... Opening file
       open(7,FILE=trim(adjustl(directory))//'/'
     .  //trim(adjustl(filename))//".vtk",FORM='formatted')

c... File header format
       write(7,'(A)') '# vtk DataFile Version 2.0' ! File header
       write(7,'(A)') 'FiniteElements VTK Output'  ! Name of simulation
       write(7,'(A)') 'ASCII'                      ! Datatype ASCII/BINARY
       write(7,'(A)') ' '

c... Node coordinates
       write(7,'(A)') 'DATASET UNSTRUCTURED_GRID'  ! Type of data, in this case a unstructured grid
       write(aux,'(I10)') nnod
       write(7,'(A)') 'POINTS ' // trim(adjustl(aux)) // ' double'      ! nnod points of float type
       do i=1,nnod
            write(aux,'(F30.10)') xnod(1,i) + disp(1,i)
            write(aux1,'(F30.10)') xnod(2,i) + disp(2,i)
            write(aux2,'(F30.10)') xnod(3,i) + disp(3,i)
            write(7,'(A)') trim(adjustl(aux))//'  '//
     .                     trim(adjustl(aux1))//'  '//
     .                     trim(adjustl(aux2))
       end do
       write(7,'(A)') ' '

c... Connectivity
       write(aux,'(I10)') nel
       write(aux1,'(I10)') nel*(n_nodes+1)
       write(7,'(A)') 'CELLS ' // trim(adjustl(aux))
     .                // ' ' // trim(adjustl(aux1))     ! nel elements and size of elnod array
       do i=1,nel
            write(aux,'(I1)') n_nodes
            write(aux1,*) elnod(:,i)-1
            write(7,'(A)') trim(adjustl(aux)) // '  '
     .                     // trim(adjustl(aux1))
       end do
       write(7,'(A)') ' '

c... VTK element type
       write(aux,'(I5)') nel
       write(7,'(A)') 'CELL_TYPES ' // trim(adjustl(aux))
       do i=1,nel
            write(aux,'(I5)') eltype
            write(7,'(A)') trim(adjustl(aux))
       end do
       write(7,'(A)') ' '

c... VTK Data
       write(7,'(A10,1x,I3)') 'POINT_DATA', nnod

       end subroutine WriteVTKDeformed

c*******************************************************************************************

       subroutine WriteVTKIntegrationPoints(directory,filename,n_coord,
     .                                      n_ip,ip_pos)

       use Mesh

       implicit none

c... Dummy Arguments
       character(LEN=*), intent(IN) :: directory, filename
       integer, intent(IN) :: n_coord, n_ip
       real(kind=8), intent(IN) :: ip_pos(n_coord,n_ip,nel)

c... Local containers
       character(LEN=100000) :: aux, aux1, aux2
       integer :: i, el, vec(n_ip*nel)

c... Opening file
       open(7,FILE=trim(adjustl(directory))//'/'
     .  //trim(adjustl(filename))//".vtk",FORM='formatted')

c... File header format
       write(7,'(A)') '# vtk DataFile Version 2.0' ! File header
       write(7,'(A)') 'FiniteElements VTK Output'  ! Name of simulation
       write(7,'(A)') 'ASCII'                      ! Datatype ASCII/BINARY
       write(7,'(A)') ' '

c... Node coordinates
       write(7,'(A)') 'DATASET UNSTRUCTURED_GRID'  ! Type of data, in this case a unstructured grid
       write(aux,'(I10)') n_ip*nel
       write(7,'(A)') 'POINTS ' // trim(adjustl(aux)) // ' double'      ! nnod points of float type
       do el=1,nel
            do i = 1,n_ip
                write(aux,'(F30.10)') ip_pos(1,i,el)
                write(aux1,'(F30.10)') ip_pos(2,i,el)
                write(aux2,'(F30.10)') 0.
                write(7,'(A)') trim(adjustl(aux)) // '  ' //
     .      trim(adjustl(aux1)) //'  '// trim(adjustl(aux2))
            end do
       end do
       write(7,'(A)') ' '

c... Connectivity
       write(aux,'(I10)') 1
       write(aux1,'(I10)') n_ip*nel+1
       write(7,'(A)') 'CELLS ' // trim(adjustl(aux))
     .                 // ' ' // trim(adjustl(aux1))     ! nel elements and size of elnod array
       do i=1, nel*n_ip
            vec(i) = i
       end do
       write(aux,'(I10)') n_ip*nel
       write(aux1,*) vec(:)-1
       write(7,'(A)') trim(adjustl(aux)) // '  ' //
     . trim(adjustl(aux1))
       write(7,'(A)') ' '

c... VTK element type
       write(aux,'(I5)') 1
       write(7,'(A)') 'CELL_TYPES ' // trim(adjustl(aux))
       write(aux,'(I5)') 2
       write(7,'(A)') trim(adjustl(aux))
       write(7,'(A)') ' '

c... VTK Data
       write(7,'(A10,1x,I5)') 'POINT_DATA', n_ip*nel

       end subroutine WriteVTKIntegrationPoints