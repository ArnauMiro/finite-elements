c*******************************************************************************************
c
c ReadSettinfs: Reads simulation settings from file
c
c*******************************************************************************************
       subroutine ReadSettings(directory)

       use System
       use Solver, only: SparseSolver

       implicit none

c... Dummy arguments
       character(LEN=*), intent(IN) :: directory

c... Local Containers
       integer i, j

c... Open settings file
       open(UNIT=4,FILE=trim(directory)//
     .  '/Settings/Settings.txt',ACCESS='SEQUENTIAL')

c... Discard first junk line
       read(4,*)

c... Read Element in use
       read(4,*) element

c... Read Solver in use
       read(4,*) solverselect
       if (solverselect .EQ. 0) then
            SparseSolver = .TRUE.
       else
            SparseSolver = .FALSE.
       end if

c... Close settings file
       close(4)

       end subroutine ReadSettings