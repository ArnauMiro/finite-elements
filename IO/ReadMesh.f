c*******************************************************************************************
c
c ReadMesh: Reads a mesh in the mesh folder of input directory in format .msh
c
c*******************************************************************************************
       subroutine ReadMesh(n_nodes,directory)

       use Mesh

       implicit none

c... Dummy arguments
       integer, intent(IN) :: n_nodes
       character(LEN=*), intent(IN) :: directory

c... Local Containers
       integer i, j

c... Open mesh file
       open(UNIT=4,FILE=trim(directory)//
     .  '/Mesh/Mesh.msh',ACCESS='SEQUENTIAL')

c... Read Nodes
       read(4,*) nnod
       allocate(xnod(3,nnod))
       do i = 1, nnod
            read(4,*) j, xnod(:,i)
            if (j .NE. i) then ! A node is missing
                print *, 'I am missing node', i
                stop
            end if
       end do

c... Read Elements
       read(4,*) nel
       allocate(elnod(n_nodes,nel))
       do i = 1, nel
            read(4,*) j, elnod(:,i)
            if (j .NE. i) then ! An element is missing
                print *, 'I am missing element', i
                stop
            end if
       end do

c... Close input file
       close(4)

       print *, 'Mesh file read successfully!'

       end subroutine ReadMesh