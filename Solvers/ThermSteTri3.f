c*******************************************************************************************
c
c Thermal Problem: Parameters and computations to solve a thermal problem
c
c*******************************************************************************************
       subroutine ThermSteTri3(directory)

c... Module dependencies
       use System
       use Mesh, only : nnod
       use ThermalSteTri3
       use Solver

       implicit none

c... Input Variables
      character(LEN=256), intent(IN) :: directory

c... Read Mesh
       call ReadMesh(n_nodes,directory)

c... Read Boundary Conditions
       call ReadBoundaries(directory)

       print *, 'Input files read, proceeding to assembly.'

c... Assembly Routines
       call ElementEval() ! Evaluates the elements
       call AssemblyGlobal() ! Assembles global matrix
       call AssemblyBoundaries() ! Assembles boundary conditions

       print*, 'System assembled, proceeding to solver.'

c... Solver
       allocate (x(ndof*nnod))
       if (SparseSolver) then
            call SparseGauss(nnod, width, width,kglob,bglob)
            x = bglob
            print*, 'System solved correctly using Sparse Gauss Solver.'
       else
        select case (solverselect)
            case(1) ! Conjugate Gradient
                call ConjugateGradient(ndof*nnod,kglob,bglob,x)
                print*, 'System solved correctly using '//
     . 'Conjugate Gradient Solver.'
            case(2) ! Gradient
                call gradient(ndof*nnod,kglob,bglob,x)
                print*, 'System solved correctly using Gradient Solver.'
            case(3) ! Gauss solver with permutations
                call PermGauss(ndof*nnod,kglob,bglob)
                x=bglob
                print*, 'System solved correctly using '//
     . 'Permutation Gauss Solver.'
            case(4) ! Gauss solver without permutations
                call Gauss(ndof*nnod,kglob,bglob)
                x=bglob
                print*, 'System solved correctly using '//
     . 'Gauss Solver.'
            case default
                print*, 'Solver not recognized, aborting.'
                stop
        end select
       end if

c... Postprocessing
c... Save temperature
       allocate( Temp(ndof*nnod) )
       Temp = x
c... Compute heat flux
       call ComputeHeatFlux(n_nodes,n_coord,n_ip,nval)

       deallocate(Kglob,bglob,x)

c... Output
c... Standard Mesh
       call WriteVTKMesh(directory,"Output",n_nodes,eltype)
       call WriteVTKScalar(directory,"Output",Temp,nnod,"Temperature")
c... Integration points
       call WriteVTKIntegrationPoints(directory,"HeatFlux",n_coord,
     .                                      n_ip,x_ip)
       call WriteVTKScalar(directory,"HeatFlux",qvec(1,:),
     .                     nel*n_ip,"qx")
       call WriteVTKScalar(directory,"HeatFlux",qvec(2,:),
     .                     nel*n_ip,"qy")
       call WriteVTKVector(directory,"HeatFlux",qvec,3,nel*n_ip,"q")

       end subroutine ThermSteTri3