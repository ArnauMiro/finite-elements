!*******************************************************************************************
!
! Thermostructural Problem: Parameters and computations to solve a thermostructural problem
!
!*******************************************************************************************
       subroutine ThermStructQuad4(directory)

c... Module dependencies
       use MeshQuad4
       use Structural, only : disp, stress, stressvec,
     .                        Thermostruct
       use Thermal, only : Temp, qvec

       implicit none

c... Input Variables
       character(LEN=256), intent(IN) :: directory

c... Setting thermostructural problem
       Thermostruct = .TRUE.

c... Read Mesh
       call ReadMesh(n_nodes,directory)

c... Solving the thermal problem
       print*, 'Solving thermal case'
       call ThermalCaseQuad4(directory)
c... Solving the structural problem
       print*, 'Solving structural case'
       call StructuralCaseQuad4(directory)

c... PostProcessing

c... Output

c... Standard Mesh
       call WriteVTKMesh(directory,"Output",n_nodes,eltype)
       call WriteVTKScalar(directory,"Output",Temp,nnod,"Temperature")
       call WriteVTKScalar(directory,"Output",disp(1,:),nnod,"Disp_x")
       call WriteVTKScalar(directory,"Output",disp(2,:),nnod,"Disp_y")
       call WriteVTKScalar(directory,"Output",disp(3,:),nnod,"Disp_z")
       call WriteVTKVector(directory,"Output",disp,3,nnod,"Disp")
c... Deformed mesh
       call WriteVTKDeformed(directory,"Deformed",n_nodes,eltype,disp)
       call WriteVTKScalar(directory,"Deformed",Temp,nnod,"Temperature")
       call WriteVTKScalar(directory,"Deformed",disp(1,:),nnod,"Disp_x")
       call WriteVTKScalar(directory,"Deformed",disp(2,:),nnod,"Disp_y")
       call WriteVTKScalar(directory,"Deformed",disp(3,:),nnod,"Disp_z")
       call WriteVTKVector(directory,"Deformed",disp,3,nnod,"Disp")
c... Integration points
       call WriteVTKIntegrationPoints(directory,"Stress",n_coord,
     .                                      n_ip,x_ip)
       call WriteVTKScalar(directory,"Stress",stressvec(1,:),
     .                     nel*n_ip,"Sxx")
       call WriteVTKScalar(directory,"Stress",stressvec(2,:),
     .                     nel*n_ip,"Syy")
       call WriteVTKScalar(directory,"Stress",stressvec(3,:),
     .                     nel*n_ip,"Sxy")
       call WriteVTKScalar(directory,"Stress",stressvec(4,:),
     .                     nel*n_ip,"Szz")
       call WriteVTKScalar(directory,"Stress",qvec(1,:),
     .                     nel*n_ip,"qx")
       call WriteVTKScalar(directory,"Stress",qvec(2,:),
     .                     nel*n_ip,"qy")
       call WriteVTKVector(directory,"Stress",qvec,3,nel*n_ip,"q")

       end subroutine ThermStructQuad4

!***************************** Thermostructural Cases *******************************************

       subroutine ThermalCaseQuad4(directory)

c... Module dependencies
       use System
       use Mesh, only : nnod
       use ThermalSteQuad4
       use Solver

       implicit none

c... Input Variables
       character(LEN=256), intent(IN) :: directory

c... Read Boundary Conditions
       call ReadBoundaries(directory)

       print *, 'Input files read, proceeding to assembly.'

c... Assembly Routines
       call ElementEval() ! Evaluates the elements
       call AssemblyGlobal() ! Assembles global matrix
       call AssemblyBoundaries() ! Assembles boundary conditions

       print*, 'System assembled, proceeding to solver.'

c... Solver
       allocate (x(ndof*nnod))
       if (SparseSolver) then
            call SparseGauss(nnod, width, width,kglob,bglob)
            x = bglob
            print*, 'System solved correctly using Sparse Gauss Solver.'
       else
        select case (solverselect)
            case(1) ! Conjugate Gradient
                call ConjugateGradient(ndof*nnod,kglob,bglob,x)
                print*, 'System solved correctly using '//
     . 'Conjugate Gradient Solver.'
            case(2) ! Gradient
                call gradient(ndof*nnod,kglob,bglob,x)
                print*, 'System solved correctly using Gradient Solver.'
            case(3) ! Gauss solver with permutations
                call PermGauss(ndof*nnod,kglob,bglob)
                x=bglob
                print*, 'System solved correctly using '//
     . 'Permutation Gauss Solver.'
            case(4) ! Gauss solver without permutations
                call Gauss(ndof*nnod,kglob,bglob)
                x=bglob
                print*, 'System solved correctly using '//
     . 'Gauss Solver.'
            case default
                print*, 'Solver not recognized, aborting.'
                stop
        end select
       end if

c... Postprocessing
c... Save temperature
       allocate( Temp(ndof*nnod) )
       Temp = x
       deallocate(Kglob,bglob,x)
c... Compute heat flux
       call ComputeHeatFlux(n_nodes,n_coord,n_ip,nval)
       deallocate(x_ip)

       end subroutine ThermalCaseQuad4

!*******************************************************************************************

       subroutine StructuralCaseQuad4(directory)

c... Module dependencies
       use System
       use Mesh, only : nnod
       use StructuralQuad4
       use Solver

       implicit none

c... Input Variables
       character(LEN=256), intent(IN) :: directory

c... Read Boundary Conditions
       call ReadBoundaries(directory)

c... Read Properties
       call ReadProperties(directory)

c... Read Stress State
       call ReadStressState(directory)

       print *, 'Input files read, proceeding to assembly.'

c... Assembly Routines
       call ElementEval() ! Evaluates the elements
       call AssemblyGlobal() ! Assembles global matrix
       call AssemblyBoundaries() ! Assembles boundary conditions

       print*, 'System assembled, proceeding to solver.'

c... Solver
       allocate (x(ndof*nnod))
       if (SparseSolver) then
            call SparseGauss(nnod, width, width,kglob,bglob)
            x = bglob
            print*, 'System solved correctly using Sparse Gauss Solver.'
       else
       select case (solverselect)
            case(1) ! Conjugate Gradient
                call ConjugateGradient(ndof*nnod,kglob,bglob,x)
                print*, 'System solved correctly using '//
     . 'Conjugate Gradient Solver.'
            case(2) ! Gradient
                call gradient(ndof*nnod,kglob,bglob,x)
                print*, 'System solved correctly using Gradient Solver.'
            case(3) ! Gauss solver with permutations
                call PermGauss(ndof*nnod,kglob,bglob)
                x=bglob
                print*, 'System solved correctly using '//
     . 'Permutation Gauss Solver.'
            case(4) ! Gauss solver without permutations
                call Gauss(ndof*nnod,kglob,bglob)
                x=bglob
                print*, 'System solved correctly using '//
     . 'Gauss Solver.'
            case default
                print*, 'Solver not recognized, aborting.'
                stop
       end select
       end if


c... PostProcessing
       call ComputeDisplacements() ! Compute displacements
       call ComputeStress(n_nodes,n_coord,n_ip,nval) ! Compute stresses
       deallocate(Kglob,bglob,x)

       end subroutine StructuralCaseQuad4
