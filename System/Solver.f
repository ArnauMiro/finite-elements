c*******************************************************************************************
c
c Solver: Contains solver subroutines and a variable to select from different solvers
c
c*******************************************************************************************
       module solver

c... Variables
       logical :: SparseSolver

c... Solver subroutines
       contains
c*******************************************************************************************

       subroutine ConjugateGradient(n,A,b,x)

       implicit none

c... Dummy variables
       integer, intent(IN) :: n
       real(kind=8), intent(IN) :: A(n,n), b(n)
       real(kind=8), intent(OUT) :: x(n)

c... Variables
       integer :: i
       real, parameter :: tol = 1.e-6
       integer, parameter :: reset_freq_r = 25, reset_freq_d = 50
       real(kind=8) :: r(n), d(n), Ad(n)
       real(kind=8) :: alpha, beta, err, rdotr, r0, rnorm
       logical cont

c... First guess using GaussSeidel
       do i=1,n
            x(i) = b(i) / A(i,i)
       end do

c... Initial residual
       r = b - matmul(A,x)
       rdotr = dot_product(r,r)

c... Inital residual norm
       r0 = sqrt(rdotr)

c... Initial Search Direction
       d = r

       cont = .TRUE.
       i=1

       do while (cont)
            Ad = matmul(A,d)
            alpha = rdotr / dot_product(d,Ad)
            x = x + alpha * d
            if (mod(i, reset_freq_r) .EQ. 1) then
                r = b - matmul(A,x)
            else
                r = r - alpha * Ad
            end if

            beta = 1. / rdotr
            rdotr = dot_product(r,r)
            beta = beta * rdotr
            rnorm = sqrt(rdotr)
            cont = rnorm .GT. (tol * r0)

            !print *, 'Iteration:', i, 'Residual Norm:', rnorm

            if (.NOT. cont) then
                !print *, 'Solution Converged!'
            else if (mod(i, reset_freq_d) .EQ. 1) then
                d = r
            else
                d = r + beta * d
            end if

            i=i+1

       end do

       err = maxval(abs(matmul(A,x) - b))
       print *, 'Final error:', err

       end subroutine conjugategradient

c*******************************************************************************************

       subroutine Gradient(n,A,b,x)

       implicit none

c... Dummy variables
       integer, intent(IN) :: n
       real(kind=8), intent(IN) :: A(n,n), b(n)
       real(kind=8), intent(OUT) :: x(n)

c... Variables
       integer :: i
       real, parameter :: tol = 1.e-4
       integer, parameter :: reset_freq = 25
       real(kind=8) :: r(n), Ar(n)
       real(kind=8) :: alpha, err
       logical cont

c... First guess using GaussSeidel
       do i=1,n
            x(i) = b(i) / A(i,i)
       end do

       cont = .TRUE.
       i = 1

       do while (cont)

c... Every 10 times or so we need to refresh the residue to avoid acummulating errors
            if ( mod(i,reset_freq) .EQ. 1) then
                r = matmul(A,x) - b
            else
                r = r - alpha * Ar
            end if

            Ar = matmul(A,r)
            alpha = dot_product(r,r) / dot_product(r,Ar)

            x = x - alpha * r

            err = maxval(abs(r)) * abs(alpha) / maxval(abs(x))
            !print *, 'Iteration:', i, 'Error:', err
            cont = err .GT. tol

            i = i + 1

       end do

       err = maxval(abs(matmul(A,x) - b))
       print *, 'Final error:', err

       end subroutine gradient

c*******************************************************************************************

       subroutine SparseGauss(n,niz,nde,A,b)

       implicit none

c... Variables parameter subroutine
       integer, intent(in) :: n, niz, nde
       real(kind=8), intent(inout) :: A(-niz:nde,n),b(n)

c... Variables
       integer pivot, row, ndown, i
       real(kind=8) mult

       do pivot=1,n-1
            ndown = min(niz,n-pivot)
            do i=1,ndown
                row = pivot + i
                ! Construction of the multiplier
                mult = A(-i,row) / A(0,pivot)
                ! Alterating rows to make zeros
                A(-i+1:nde-i,row) = A(-i+1:nde-i,row)
     .                              - mult*A(1:nde,pivot)
                b(row) = b(row) - mult*b(pivot)
            end do
       end do

c... Calculating the solutions
       b(n) = b(n) / A(0,n) ! this line is not really needed!!!
       do row=n-1,1,-1
            ndown = min(nde,n-row)
            ! We make use of the dot product between two vectors to compute the terms that are known
            b(row) = ( b(row) - dot_product(A(1:ndown,row),
     .               b(row+1:row+ndown)) ) / A(0,row)
       end do

       end subroutine SparseGauss

!*******************************************************************************************

       subroutine PermGauss(n,A,b)

       implicit none

c... Variables parameter subroutine
       integer, intent(in) :: n
       real(kind=8), intent(inout) :: A(n,n),b(n)

c... Variables
       integer pivot, row
       real(kind=8) mult, max

c... Pointers
       integer point(n), posmax, aux

c... Pointer inicialization
       do row=1,n
            point(row) = row
       end do

c... Solver Loop
       do pivot=1,n-1
            ! Locate the row with the maximum and put it on top
            max = abs(A(point(pivot),pivot))
            posmax = pivot

            do row=pivot+1,n
                if(abs(A(point(row),pivot)) .gt. max) then
                    max = abs(A(point(row),pivot))
                    posmax = row
                end if
            end do

            ! Permute the pointer value
            aux = point(posmax)
            point(posmax) = point(pivot)
            point(pivot) = aux

            do row=pivot+1,n
                ! Construction of the multiplier
                mult = A(point(row),pivot) / A(point(pivot),pivot)
                ! Alterating rows to make zeros
                A(point(row),pivot+1:n) = A(point(row),pivot+1:n) -
     .                               mult*A(point(pivot),pivot+1:n)
                b(point(row)) = b(point(row)) - mult*b(point(pivot))
            end do
        end do

c... Calculating the solutions
       b(point(n)) = b(point(n)) / A(point(n),n) ! this line is not really needed!!!
       do row=n-1,1,-1
            ! We make use of the dot product between two vectors to compute the terms that
            ! are known
            b(point(row)) = ( b(point(row)) - dot_product(A(point(row),
     .                row+1:n),b(point(row)+1:n)) ) / A(point(row),row)
       end do

       end subroutine PermGauss

c*******************************************************************************************

       subroutine Gauss(n,A,b)

       implicit none

c... Variables parameter subroutine
       integer, intent(in) :: n
       real(kind=8), intent(inout) :: A(n,n),b(n)

c... Variables
       integer pivot, row
       real(kind=8) mult

       do pivot=1,n-1
            do row=pivot+1,n
                ! Construction of the multiplier
                mult = A(row,pivot) / A(pivot,pivot)
                ! Alterating rows to make zeros
                A(row,pivot+1:n) = A(row,pivot+1:n) -
     .                         mult*A(pivot,pivot+1:n)
                b(row) = b(row) - mult*b(pivot)
            end do
       end do

c... Calculating the solutions
       b(n) = b(n) / A(n,n) ! this line is not really needed!!!
       do row=n-1,1,-1
            ! We make use of the dot product between two vectors to compute the terms that
            ! are known
            b(row) = ( b(row) - dot_product(A(row,row+1:n),
     .                           b(row+1:n)) ) / A(row,row)
       end do

       end subroutine Gauss

       end module solver