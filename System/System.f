c*******************************************************************************************
c
c System: Contains global variables and settings for the code
c
c*******************************************************************************************
       module System

       implicit none

c... Generic Variables
       real(kind=8), allocatable, save :: kglob(:,:), bglob(:), x(:)
       integer, save :: width

c... Settings Variables
       integer, save :: element ! Element selector
       integer, save ::  solverselect ! Selection of solver

       end module System