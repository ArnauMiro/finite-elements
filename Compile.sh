#! bin/bash

# Compilation file routine for the FESuite
# Generates the executables in the 'Release' folder and copies them to the system folder
# Uses the makefiles included in the code

clear

echo 'Compiling the Finite Elements suite...'
echo '--------------------------------------'

# Create binaries folder
echo 'Remmoving previous instances...'
rm -rf Release/bin/

echo 'Setting binaries...'
mkdir Release/bin

echo 'Initiating compilation...'
echo ' '

# Compiling system
cp System/*.f Release/bin
cp System/makefile Release/bin

cd Release/bin

make

rm *.f makefile

cd ../..

echo 'System modules compiled sucessfully'
echo '--------------------------------------'

# Compiling elements
cp Mesh/Elements/*.f Release/bin
cp Mesh/Elements/makefile Release/bin

cd Release/bin

make

rm *.f makefile

cd ../..

echo 'Element modules compiled sucessfully'
echo '--------------------------------------'

# Compiling meshes
cp Mesh/*.f Release/bin
cp Mesh/makefile Release/bin

cd Release/bin

make

rm *.f makefile

cd ../..

echo 'Mesh modules compiled sucessfully'
echo '--------------------------------------'

# Compiling cases
cp Cases/*.f Release/bin
cp Cases/makefile Release/bin

cd Release/bin

make

rm *.f makefile

cd ../..

echo 'Cases compiled sucessfully'
echo '--------------------------------------'

# Compiling IO functions
cp IO/*.f Release/bin
cp IO/makefile Release/bin

cd Release/bin

make

rm *.f makefile

cd ../..

echo 'IO functions compiled sucessfully'
echo '--------------------------------------'

# Compiling Solver functions
cp Solvers/*.f Release/bin
cp Solvers/makefile Release/bin

cd Release/bin

make

rm *.f makefile

cd ../..

echo 'Solver functions compiled sucessfully'
echo '--------------------------------------'

# Compiling Executables
cp Executables/*.f Release/bin
cp Executables/makefile Release/bin

cd Release/bin

make

rm *.f makefile

cd ../..

echo 'Executables compiled sucessfully'
echo '--------------------------------------'

# Building Executables
cp Executables/MakefileThermal Release/bin
cp Executables/MakefileStructural Release/bin
cp Executables/MakefileThermoStructural Release/bin

cd Release/bin

echo 'Building FEThermSte...'

mv MakefileThermal Makefile

make

echo 'Building FEStructural...'

mv MakefileStructural Makefile

make

echo 'Building FEThermoStructural...'

mv MakefileThermoStructural Makefile

make

rm Makefile

cd ../..

echo 'Executables built sucessfully'
echo '--------------------------------------'

# Building Mesh Tool
cp Mesh/MeshTools/*.f Release/bin
cp Mesh/MeshTools/*c Release/bin

cd Release/bin

echo 'Building EasyMesh...'
gcc -w easymesh.c -o EasyMesh

echo 'Building FEMeshTool...'
gfortran FEMeshTool.f -o FEMeshTool

rm *.f *.c

cd ../..

echo 'Mesh tools built sucessfully'
echo '--------------------------------------'

# Clean Up
echo -ne 'Cleaning... '

cd Release/bin
rm *.o *.mod

echo 'Cleaned'
cd ../..