c*******************************************************************************************
c
c MeshThermal: Thermal assembly subroutines and boundary conditions
c
c*******************************************************************************************
       module Thermal

c... Other submodule dependencies
       use System

       implicit none

c... Thermal Problem Variables
       real(kind=8), allocatable, save :: Temp(:), q(:,:,:), qvec(:,:)
       real(kind=8), save :: T0, alpha

c... Boundary Variables
       integer, save :: nfix, nflux
       integer, parameter :: ndof = 1
       real(kind=8), allocatable :: bfix(:), bflux(:)
       integer, allocatable :: fixnod(:), fixflux(:,:)

c... Assembly subroutines
       contains
c*******************************************************************************************

       subroutine ReadBoundaries(directory)

       implicit none

c... Dummy arguments
       character(LEN=*), intent(IN) :: directory

c... Local Containers
       integer i, j

c... Open Boundary File
       open(UNIT=4,FILE=trim(directory)//
     . '/Mesh/Boundaries.txt',ACCESS='SEQUENTIAL')

c... Read Fixed Boundaries
       read(4,*) nfix
       allocate(fixnod(nfix), bfix(nfix))
       do i = 1, nfix
            read(4,*) j, fixnod(i), bfix(i)
            if (j .NE. i) then ! A node is missing
                print *, 'I am missing fixed node', i
                stop
            end if
       end do

c... Read Flux Boundaries
       read(4,*) nflux
       allocate(fixflux(2,nflux), bflux(nflux))
       do i = 1, nflux
            read(4,*) j, fixflux(:,i), bflux(i)
            if (j .NE. i) then ! A face is missing
                print *, 'I am missing face', i
                stop
            end if
       end do

c... Close Boundaries File
       close(4)

       print *, 'Boundaries file read successfully!'

       end subroutine ReadBoundaries

       end module Thermal