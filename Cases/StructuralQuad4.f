c*******************************************************************************************
c
c Structural: Structural assembly subroutines and boundary conditions
c
c*******************************************************************************************
       module StructuralQuad4

c... Other submodule dependencies
       use Structural
       use MeshQuad4
       use Solver, only : SparseSolver

c... Assembly subroutines
       contains
!*******************************************************************************************

       subroutine AssemblyGlobal()

       implicit none

c... Variables
       integer :: el
       real(kind=8) :: Kel(n_nodes*ndof,n_nodes*ndof)

       call EvalDMatrix()

       if (SparseSolver) then
            width = 0

            do el = 1, nel
                width = max(width, maxval(elnod(:,el)) -
     .                  minval(elnod(:,el)))
            end do

            allocate( kglob(-width:width, nnod) )
       else
            allocate( Kglob(nnod*ndof,nnod*ndof) )
       end if

       Kglob = 0.

c... Global Matrix assembly
       do el = 1,nel

            call BuildKelem(el,Kel)

            if (SparseSolver) then
                call AssemblyElementSparse(Kel, Kglob,
     .                     elnod(:,el), n_nodes, nnod)
            else
                call AssemblyElement(Kel, Kglob,
     .                elnod(:,el), n_nodes, nnod, ndof)
            end if
       end do

       end subroutine AssemblyGlobal

c*******************************************************************************************

       subroutine BuildKelem(el,Kel)

       implicit none

       real(kind=8), intent(OUT):: Kel(n_nodes*ndof,n_nodes*ndof)
       integer, intent(IN) :: el

c... Variables
       integer :: i, j, ip
       real(kind=8) :: pos(n_coord,n_nodes), der(n_coord,n_nodes,n_ip),
     .                 ip_pos(n_coord,n_ip), A(n_ip), B(3,n_nodes*ndof)

       do i = 1,n_nodes
            pos(:,i) = xnod(1:2,elnod(i,el))
       end do

c... Isoparametric transformation
       call IsopTrans(pos,der, A, ip_pos)

c... CHECK if element is inverted
       if ( minval(A) .LT. 0) then
            print*, 'WARNING: Element', el,'is upside down'
            stop
       end if

       Kel  = 0.

       do ip=1,n_ip

            call EvalBMatrix(B,ip,der)

            Kel = Kel + matmul(transpose(B), matmul(D, B)) * A(ip)
       end do

       end subroutine BuildKelem

c*******************************************************************************************

       subroutine EvalBMatrix(B,ip,der)

       implicit none

c... Dummy arguments
       integer, intent(IN) :: ip
       real(kind=8), intent(IN) :: der(n_coord,n_nodes,n_ip)
       real(kind=8), intent(OUT) :: B(3,n_nodes*ndof)

       integer :: i

       B(1:2,:) = 0.

       do i = 1,n_nodes
c... First line x derivative
            B(1,2*i-1) = der(1,i,ip)

c... Second line y derivatives
            B(2,2*i) = der(2,i,ip)

c... Third line cross derivatives
            B(3,2*i-1) = der(2,i,ip)
            B(3,2*i) = der(1,i,ip)
       end do

       end subroutine EvalBMatrix

c*******************************************************************************************

       subroutine EvalDMatrix()

       implicit none

       real(kind=8) :: aux

       D(1:2,3) = 0.
       D(3,1:2) = 0.
       D(2,1) = poisson
       D(1,2) = poisson

       select case (StressState)
            case(1) ! Plane Stress
                print *, 'Plane Stress Analysis'
                D(1,1) = 1.
                D(2,2) = 1.
                D(3,3) = 0.5 * (1. - poisson)

                aux = young / (1. - poisson * poisson)
            case(2) ! Plane Strain
                print *, 'Plane Strain Analysis'
                D(1,1) = 1. - poisson
                D(2,2) = D(1,1)
                aux = D(2,2) - poisson
                D(3,3) = 0.5 * aux

                aux = young / ((1. + poisson) * aux)
            case default
                print *, 'No Stress Case Selected. Aborting.'
                stop
       end select

       D = aux * D

       end subroutine EvalDMatrix

c*******************************************************************************************

       subroutine AssemblyBoundaries()

       implicit none

c... Variables
       integer :: side, n1, n2, i, nloc
       real(kind=8) :: vec(2), long, aux

c... Assembly routine
       allocate ( bglob(ndof*nnod) )
       bglob = 0.

c... Fixed force
       do side = 1,nload
            n1 = fixload(side)
            nloc = ( n1-1 ) * ndof + 1

            bglob(nloc) = bglob(nloc) + load(1,side)
            bglob(nloc+1) = bglob(nloc+1) + load(2,side)
        end do

c... Thermal Load
       if (Thermostruct) then
            call EvalTempForces()
       end if

c... Fixed displacement
       do i = 1,nfix
            n1 = fixnod(i)
            nloc = ( n1-1 ) * ndof + 1

            if (SparseSolver) then
                aux = kglob(0,n1)
                kglob(:,n1) = 0.
                kglob(0,n1) = aux
            else
                aux = Kglob(nloc,nloc)
                kglob(nloc,:) = 0.
                kglob(nloc,nloc) = aux
                aux = Kglob(nloc+1,nloc+1)
                kglob(nloc+1,:) = 0.
                kglob(nloc+1,nloc+1) = aux
            end if
            bglob(nloc) = aux * bfix(1,i)
            bglob(nloc+1) = aux * bfix(2,i)
       end do

       end subroutine AssemblyBoundaries

c*******************************************************************************************

       subroutine EvalTempForces()

       implicit none

       integer :: i, ip, el, iloc
       real(kind=8) :: T, I_mat(3), fel(ndof*n_nodes),
     .  pos(n_coord,n_nodes), der(n_coord,n_nodes,n_ip),
     .  ip_pos(n_coord,n_ip), A(n_ip), B(3,n_nodes*ndof)

       I_mat = 0.
       I_mat(1:2) = 1.

       do el = 1,nel
            do i = 1,n_nodes
                pos(:,i) = xnod(1:2,elnod(i,el))
            end do

c... Isoparametric transformation
            call IsopTrans(pos,der, A, ip_pos)

            fel  = 0.

c... Element temp load computation
            do ip=1,n_ip
                T = 0.
                call EvalBMatrix(B,ip,der)

c... Temperature in integration points
                do i=1,n_nodes
                    T = T + nval(i,ip) * Temp(elnod(i,el))
                end do
                fel = fel + alpha*T0*T*
     .        matmul( transpose(B),matmul(D,I_mat) )*A(ip)
           end do

c... Assembly of fel
           do i=1,n_nodes
                iloc = (elnod(i,el)-1)*ndof + 1
                bglob(iloc:iloc+1) = bglob(iloc:iloc+1) + fel(2*i-1:2*i)
           end do
       end do

       end subroutine EvalTempForces

c*******************************************************************************************

       subroutine ComputeStress(n_nodes,n_coord,n_ip,nval)

c... Modules
       use Mesh

       implicit none

c... Dummy arguments
       integer, intent(IN) :: n_nodes, n_coord, n_ip
       real(kind=8), intent(IN) :: nval(n_nodes,n_ip)

c... Variables
       integer :: i, ip, el
       real(kind=8) :: T, I_mat(3), pos(n_coord,n_nodes),
     . desp(ndof*n_nodes),
     . der(n_coord,n_nodes,n_ip),A(n_ip), B(3,n_nodes*ndof)

       allocate( stress(4,n_ip,nel) )
       allocate( x_ip(n_coord,n_ip,nel) )

       I_mat = 0.
       I_mat(1:2) = 1.

       do el=1,nel
c... Displacement of the nodes of the element
            do i = 1,n_nodes
                pos(:,i) = xnod(1:2,elnod(i,el))
                desp(2*i-1) = disp(1,elnod(i,el))
                desp(2*i) = disp(2,elnod(i,el))
            end do
c... Isoparametric transformation
            call IsopTrans(pos,der, A, x_ip(:,:,el))
            do ip =1, n_ip
                call EvalBMatrix(B,ip,der)
                T = 0.
                if (Thermostruct) then
                    do i = 1,n_nodes
                        T = T + nval(i,ip)*Temp(elnod(i,el))
                    end do
                end if
                if (StressState .EQ. 2) then ! Plane strain
                    stress(1:3,ip,el) = matmul( D, matmul(B,desp) )
     .                     - (1+poisson)*alpha*T0*T*matmul(D,I_mat)
                    stress(4,ip,el) = poisson*( stress(1,ip,el) +
     .           stress(2,ip,el) ) - young*alpha*T0*T/(1-2*poisson)
                else
                    stress(1:3,ip,el) = matmul( D, matmul(B,desp) )
     .                                 - alpha*T0*T*matmul(D,I_mat)
                end if
            end do
       end do

       allocate( stressvec(4,nel*n_ip) )
       stressvec = 0.
       i = 0
       do el=1,nel
            do ip = 1,n_ip
                i = i + 1
                stressvec(1,i) = stress(1,ip,el)
                stressvec(2,i) = stress(2,ip,el)
                stressvec(3,i) = stress(3,ip,el)
                stressvec(4,i) = stress(4,ip,el)
            end do
       end do

       end subroutine ComputeStress

       end module StructuralQuad4