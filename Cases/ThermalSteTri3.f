c*******************************************************************************************
c
c MeshThermal: Thermal assembly subroutines and boundary conditions
c
c*******************************************************************************************
       module ThermalSteTri3

c... Other submodule dependencies
       use Thermal
       use MeshTri3
       use Solver, only : SparseSolver

c... Assembly subroutines
       contains
c*******************************************************************************************

       subroutine AssemblyGlobal()

       implicit none

c... Variables
       integer :: el
       real(kind=8) :: Kel(n_nodes*ndof,n_nodes*ndof)

       if (SparseSolver) then
            width = 0

            do el = 1, nel
                width = max(width, maxval(elnod(:,el))
     .                  - minval(elnod(:,el)))
            end do

            allocate( kglob(-width:width, nnod) )
       else
            allocate( Kglob(nnod*ndof,nnod*ndof) )
       end if

       Kglob = 0.

c... Global Matrix assembly
       do el = 1,nel

            call BuildKelem(el,Kel)

            if (SparseSolver) then
                call AssemblyElementSparse(Kel, Kglob,
     .                        elnod(:,el), n_nodes, nnod)
            else
                call AssemblyElement(Kel, Kglob,
     .                  elnod(:,el), n_nodes, nnod, ndof)
       end if

       end do

       end subroutine AssemblyGlobal

c*******************************************************************************************

       subroutine BuildKelem(el,Kel)

       implicit none

c... Dummy arguments
       integer, intent(IN) :: el
       real(kind=8), intent(INOUT) :: Kel(n_nodes*ndof,n_nodes*ndof)

c... Variables
       integer :: i, j, ip
       real(kind=8) :: pos(n_coord,n_nodes), der(n_coord,n_nodes,n_ip),
     .                 ip_pos(n_coord,n_ip), A(n_ip)

       do i = 1,n_nodes
            pos(:,i) = xnod(1:2,elnod(i,el))
       end do

c... Isoparametric transformation
       call IsopTrans(pos,der, A, ip_pos)

c... CHECK if element is inverted
       if ( minval(A) .LT. 0) then
            print*, 'WARNING: Element', el,'is upside down'
            stop
       end if

c... Element stiffness matrix
       do i = 1,n_nodes
            do j = 1,n_nodes
                Kel(i,j) = 0.
                do ip = 1,n_ip
                    Kel(i,j) = Kel(i,j) + ( der(1,i,ip)*der(1,j,ip)
     .                          + der(2,i,ip)*der(2,j,ip) ) * A(ip)
                end do
            end do
       end do

       end subroutine BuildKelem

c*******************************************************************************************

       subroutine AssemblyBoundaries()

       implicit none

c... Variables
       integer :: side, n1, n2, i
       real(kind=8) :: vec(2), long, aux

c... Assembly routine
       allocate ( bglob(nnod) )
       bglob = 0.

c... Neumann boundary condition
       do side = 1,nflux
            n1 = fixflux(1,side)
            n2 = fixflux(2,side)

            vec = xnod(:,n2) - xnod(:,n1)
            long = sqrt(vec(1)*vec(1) + vec(2)*vec(2))

            aux = 0.5 * long * bflux(side)

            bglob(n1) = bglob(n1) + aux
            bglob(n2) = bglob(n2) + aux
       end do

c... Dirichlet boundary condition
       do i = 1,nfix
            n1 = fixnod(i)

            if (SparseSolver) then
                aux = kglob(0,n1)
                kglob(:,n1) = 0.
                kglob(0,n1) = aux
            else
                aux = Kglob(n1,n1)
                kglob(n1,:) = 0.
                kglob(n1,n1) = aux
            end if

            bglob(n1) = aux * bfix(i)

       end do

       end subroutine AssemblyBoundaries

c*******************************************************************************************

       subroutine ComputeHeatFlux(n_nodes,n_coord,n_ip,nval)

c... Modules
       use Mesh

       implicit none

c... Dummy arguments
       integer, intent(IN) :: n_nodes, n_coord, n_ip
       real(kind=8), intent(IN) :: nval(n_nodes,n_ip)

c... Variables
       integer :: i, ip, el
       real(kind=8) :: pos(n_coord,n_nodes),
     . der(n_coord,n_nodes,n_ip),A(n_ip)

       allocate( q(2,n_ip,nel) )
       allocate( x_ip(n_coord,n_ip,nel) )

       q = 0.

       do el=1,nel
c... Displacement of the nodes of the element
            do i = 1,n_nodes
                pos(:,i) = xnod(1:2,elnod(i,el))
            end do
c... Isoparametric transformation
            call IsopTrans(pos,der, A, x_ip(:,:,el))
c... Computation of heat flux
            do ip =1, n_ip
                do i = 1, n_nodes
                    q(1,ip,el) = q(1,ip,el) - der(1,i,ip)
     .                      *nval(i,ip)*Temp(elnod(i,el))
                    q(2,ip,el) = q(2,ip,el) - der(2,i,ip)
     .                      *nval(i,ip)*Temp(elnod(i,el))
                end do
            end do
       end do

       allocate( qvec(3,nel*n_ip) )
       qvec = 0.
       i = 0
       do el=1,nel
            do ip = 1,n_ip
                i = i + 1
                qvec(1,i) = qvec(1,i) + q(1,ip,el)
                qvec(2,i) = qvec(2,i) + q(2,ip,el)
            end do
       end do

       end subroutine ComputeHeatFlux

       end module ThermalSteTri3