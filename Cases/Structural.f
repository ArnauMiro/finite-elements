c*******************************************************************************************
c
c Structural: Structural assembly subroutines and boundary conditions
c
c*******************************************************************************************
       module Structural

c... Other submodule dependencies
       use System
       use Thermal, only : Temp, T0, alpha

       implicit none

c... Structural Problem
       real(kind=8), save :: poisson, young, D(3,3)
       real(kind=8), allocatable, save :: disp(:,:), stress(:,:,:),
     .                                stressvec(:,:)
        ! 1: Sxx   2: Syy   3: Sxy   4: Szz

c... Boundary Variables
       integer, save :: nfix, nload, StressState
       integer, parameter :: ndof = 2
       real(kind=8), allocatable :: bfix(:,:), load(:,:)
       integer, allocatable :: fixnod(:), fixload(:)

c... Thermostructural flag
       logical, save :: Thermostruct

c... Assembly subroutines
       contains
c*******************************************************************************************

       subroutine ComputeDisplacements()

c... Modules
       use Mesh, only: nnod

       implicit none

c... Variables
       integer :: i

       allocate( disp(3,nnod) )

       disp = 0.

       do i=1,nnod
            disp(1,i) = x(2*i-1)
            disp(2,i) = x(2*i)
       end do

       end subroutine ComputeDisplacements

c*******************************************************************************************

       subroutine ReadBoundaries(directory)

       implicit none

c... Dummy arguments
       character(LEN=*), intent(IN) :: directory

c... Local Containers
       integer i, j, aux

c... Open Boundary File
       open(UNIT=4,FILE=trim(directory)//
     . '/Mesh/Boundaries.txt',ACCESS='SEQUENTIAL')

c... If ThermoStructural problem, skip the first lines
       if (Thermostruct) then
c... Skip fixed temperatures
            read(4,*) aux
            do i=1, aux
                read(4,*)
            end do
c... Skip flux temperatures
            read(4,*) aux
            do i=1, aux
                read(4,*)
            end do
       end if

c... Read Fixed Boundaries
       read(4,*) nfix
       allocate(fixnod(nfix), bfix(ndof,nfix))
       do i = 1, nfix
            read(4,*) j, fixnod(i), bfix(:,i)
            if (j .NE. i) then ! A node is missing
                print *, 'I am missing fixed node', i
                stop
            end if
       end do

c... Read Flux Boundaries
       read(4,*) nload
       allocate(fixload(nload), load(ndof,nload))
       do i = 1, nload
            read(4,*) j, fixload(i), load(:,i)
            if (j .NE. i) then ! A face is missing
                print *, 'I am missing face', i
                stop
            end if
       end do

c... Close Boundaries File
       close(4)

       print *, 'Boundaries file read successfully!'

       end subroutine ReadBoundaries

c*******************************************************************************************

       subroutine ReadStressState(directory)

       implicit none

c... Dummy arguments
       character(LEN=*), intent(IN) :: directory

c... Open settings file
       open(UNIT=4,FILE=trim(directory)//
     .  '/Settings/Settings.txt',ACCESS='SEQUENTIAL')

c... Discard the 3 first lines
       read(4,*)
       read(4,*)
       read(4,*)

c... Read stress state
       read(4,*) StressState

c... Close settings file
       close(4)

       end subroutine ReadStressState

c*******************************************************************************************

       subroutine ReadProperties(directory)

       implicit none

c... Dummy arguments
       character(LEN=*), intent(IN) :: directory

c... Open settings file
       open(UNIT=4,FILE=trim(directory)//
     .  '/Settings/Properties.txt',ACCESS='SEQUENTIAL')

c... Discard the first line
       read(4,*)

c... Read Alpha
       read(4,*), alpha

c... Read T0
       read(4,*), T0

c... Read Young's Modulus
       read(4,*), young

c... Poisson Ratio
       read(4,*), poisson

c... Close settings file
       close(4)

       end subroutine ReadProperties

       end module Structural