c*******************************************************************************************
c
c FINITE ELEMENTS: A finite elements code programmed in Fortran77
c
c FEMeshTool: A 2D Tri3 mesher tool using EasyMesh
c
c Programmed by: Arnau Miro
c
c*******************************************************************************************

       program FEMeshTool

       implicit none

c... Variables
       integer :: nnod, nel, nbound
       real(kind=8), allocatable :: xnod(:,:)
       integer, allocatable :: elnod(:,:), marker(:)
       character(LEN=256) :: dir, name

       integer :: i, j

c... Get current directory
       call getcwd(dir)

c... Ask for case name
       print*, 'Enter input file name: '
       read*, name

c... Run EasyMesh
       call System('EasyMesh '//trim(adjustl(dir))//
     .             '/Mesh/'//trim(adjustl(name)))

c... Get Node coordinates from .n file
c... Open mesh file
       open(4,FILE=trim(adjustl(dir))//'/Mesh/'
     .  //trim(adjustl(name))//".n",ACCESS='SEQUENTIAL')

c... Get number of nodes
       read(4,*) nnod

c... Allocate to xnod
       allocate(xnod(2,nnod),marker(nnod))

c... Read node coordinates
       do i = 1, nnod
            read(4,*) j, xnod(:,i), marker(i)
            if (j+1 .NE. i) then ! An element is missing
                print *, 'I am missing element', i
                stop
            end if
       end do

c... Close file
      close(4)

c... Get Element connectivity from .e file
c... Open mesh file
       open(4,FILE=trim(adjustl(dir))//'/Mesh/'
     .  //trim(adjustl(name))//".e",ACCESS='SEQUENTIAL')

c... Get number of elements
       read(4,*) nel

c... Allocate to xnod
       allocate(elnod(3,nel))

c... Read node coordinates
       do i = 1, nel
            read(4,*) j, elnod(:,i)
            if (j+1 .NE. i) then ! An element is missing
                print *, 'I am missing face', i
                stop
            end if
       end do

c... Close file
      close(4)

c... Write Mesh file
c... Write .msh file
       open(7,FILE=trim(adjustl(dir))//'/Mesh/'
     .  //"Mesh.msh",ACCESS='SEQUENTIAL')

c... Write (x,y) nodes
       write(7,*) nnod
       do i = 1, nnod
            write(7,*) i, xnod(:,i), 0
       end do

c... Write connectivity matrix
       write(7,*) nel
       do i = 1, nel
            write(7,*) i, elnod(:,i)+1
       end do

c... Close file
       close(7)

c... Write Boundary file
c... Write .b file
       open(7,FILE=trim(adjustl(dir))//'/Mesh/'
     .  //"Mesh.b",ACCESS='SEQUENTIAL')

c... Write boundary nodes
       nbound = 0
       do i = 1, nnod
            if (marker(i) .GT. 1) then
                nbound = nbound + 1
            end if
       end do
       write(7,*) nbound
       do i = 1, nnod
            if (marker(i) .GT. 1) then
                write(7,*) i, marker(i)
            end if
       end do

c... Close file
       close(7)

       end program FEMeshTool