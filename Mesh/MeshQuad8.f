c*******************************************************************************************
c
c Mesh: Contains node positions and connectivity matrix, which is the same for every problem
c       Also contains element assembly variables
c
c*******************************************************************************************
       module MeshQuad8

       use System
       use Mesh

c... Define the type of element to be used by the code
       use Quad8

       contains
c*******************************************************************************************

       subroutine AssemblyElement(K_el, K_glob, nodes, n_nel,
     .                            ntot, n_ndof)

       implicit none

c... Dummy arguments
       integer, intent(IN) :: n_nel, ntot, n_ndof
       integer, intent(IN) :: nodes(n_nel)
       real(kind=8), intent(IN) :: K_el(n_nel*n_ndof,n_nel*n_ndof)
       real(kind=8), intent(INOUT) :: K_glob(ntot*n_ndof,ntot*n_ndof)

c... Variables
       integer :: i, iloc, j, jloc, row, col, aux

       aux = n_ndof - 1

c... Assembly routine
       do i=1,n_nel
            do j=1,n_nel
                row = ( nodes(i)-1 ) * n_ndof + 1
                col = ( nodes(j)-1 ) * n_ndof + 1

                iloc = ( i-1 ) * n_ndof + 1
                jloc = ( j-1 ) * n_ndof + 1

                K_glob(row:row+aux, col:col+aux) =
     .          K_glob(row:row+aux, col:col+aux)
     .          + K_el(iloc:iloc+aux, jloc:jloc+aux)
            end do
       end do

       end subroutine AssemblyElement

c*******************************************************************************************

       subroutine AssemblyElementSparse(K_el, K_glob, nodes,
     .                                  n_nel, ntot)

       implicit none

c... Dummy Arguments
       integer, intent (IN) :: n_nel, ntot
       integer, intent (IN) :: nodes(n_nel)
       real(kind=8), intent (IN) :: K_el(n_nel, n_nel)
       real(kind=8), intent (INOUT) :: K_glob(-width:width, ntot)

c... Local Containers
       integer :: i, j, row, col

       do i = 1, n_nel
            do j = 1, n_nel
                row = nodes(i)
                col = nodes(j) - row
                K_glob(col, row) = K_glob(col, row) + K_el(i, j)
            end do
       end do

       end subroutine AssemblyElementSparse

       end module MeshQuad8