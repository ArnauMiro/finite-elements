c*******************************************************************************************
c
c Mesh: Contains node positions and connectivity matrix, which is the same for every problem
c
c*******************************************************************************************
       module Mesh

       implicit none

       integer, save :: nnod, nel
       real(kind=8), allocatable :: xnod(:,:),x_ip(:,:,:)
       integer, allocatable :: elnod(:,:)

       end module Mesh