c*******************************************************************************************
c
c Quad4: Contains all the functions available for a quad4 (linear quadrilateral) element and
c        loads the element parameters
c
c*******************************************************************************************
       module Quad4

       implicit none

c... Generic integers that define the dimensions of the element
       integer, parameter :: n_nodes = 4 ! number of nodes in an element
       integer, parameter :: n_coord = 2 ! x,y or chi,eta coordinates
       integer, parameter :: n_ip = 4 ! number of integration points

c... Variables that define a Quad4 element
       real(kind=8), save :: nval(n_nodes,n_ip)
       real(kind=8), save ::ngrad(n_coord,n_nodes,n_ip)
       real(kind=8), save :: w_ip(n_ip)
       real(kind=8), save ::ip_cord(n_coord,n_ip)

c... Output variables
       integer, parameter :: eltype = 9 !VTK element type

c... Subroutines for the evaluation and isoparametric transformation of a Quad4 element
       contains
c*******************************************************************************************

       subroutine ElementEval()

       implicit none

c... Variables
       real(kind=8), parameter :: ip_pos = 1. / sqrt(3.), quarter = 0.25
       real(kind=8) :: chi, eta
       integer :: i

c... Integration point position in (xi,eta)
       ip_cord = ip_pos
       !Setting all negatives
       ip_cord(:,1) = -ip_pos
       ip_cord(2,2) = -ip_pos
       ip_cord(1,4) = -ip_pos

c... Gauss integration point weight
       w_ip = 1.

       do i = 1, n_ip
c... Local coordinates
            chi = ip_cord(1,i)
            eta = ip_cord(2,i)

c... Value of the shape function in (xi,eta) coordinates
            nval(1,i) = (1. - chi) * (1. - eta)
            nval(2,i) = (1. + chi) * (1. - eta)
            nval(3,i) = (1. + chi) * (1. + eta)
            nval(4,i) = (1. - chi) * (1. + eta)

c... Value of the gradient of the shape function in (xi,eta) coordinates
            ngrad(1,1,i) = eta - 1.
            ngrad(2,1,i) = chi - 1.

            ngrad(1,2,i) = 1. - eta
            ngrad(2,2,i) = -chi -1

            ngrad(1,3,i) = 1. + eta
            ngrad(2,3,i) = 1. + chi

            ngrad(1,4,i) = -1. - eta
            ngrad(2,4,i) = 1. - chi
       end do

       nval = quarter * nval
       ngrad = quarter * ngrad

       end subroutine ElementEval

c*******************************************************************************************

       subroutine IsopTrans(pos,der, A, ip_pos)

       implicit none

c... Variables
       real(kind=8), intent(IN) :: pos(n_coord,n_nodes)
       real(kind=8), intent(OUT) :: der(n_coord,n_nodes,n_ip), A(n_ip)
       real(kind=8), intent(OUT) :: ip_pos(n_coord,n_ip)

c... Subroutine variables
       real(kind=8) :: aux, J(n_coord,n_coord), Jinv(n_coord,n_coord)
       integer :: i

c... Computation of the Jacobian
       do i=1,n_ip
c... Compute jacobian for each integration point
            J = matmul(ngrad(:,:,i), transpose(pos))
            aux = J(1,1)*J(2,2) - J(2,1)*J(1,2) ! det(J)

c... Area
            A(i) = aux * w_ip(i)

c... Inverse
            aux = 1. / aux
            Jinv(1,1) = J(2,2) * aux
            Jinv(2,2) = J(1,1) * aux
            Jinv(1,2) = -J(1,2) * aux
            Jinv(2,1) = -J(2,1) * aux

c... Compute of derivatives, global shape gradients
            der(:,:,i) = matmul( Jinv,ngrad(:,:,i) )

c... Coordinates (x,y) of the integration points
            ip_pos(:,i) = matmul( pos(:,:),nval(:,i) )

       end do

       end subroutine IsopTrans

       end module Quad4