#! bin/bash

# Uninstall file routine for the FESuite
# Removes binaries from system file

clear

echo 'Removing the Finite Elements suite...'
echo '--------------------------------------'

# Remove FEThermSte
echo 'Removing FEThermSte...'
rm /usr/bin/FEThermSte

# Remove FEStructural
echo 'Removing FEStructural...'
rm /usr/bin/FEStructural

# Remove FEThermoStructural
echo 'Removing FEThermoStructural...'
rm /usr/bin/FEThermoStructural

# Remove FEMeshTools
echo 'Removing FEMeshTools...'
rm /usr/bin/EasyMesh
rm /usr/bin/FEMeshTool

# Removing binaries
echo 'Removing binaries...'
rm -rf Release/bin/

# End
echo ' '
echo 'FESuite removed completely.'